import 'package:flutter/material.dart';
import 'package:gardesh_pay/views/splash_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'gardesh Pay',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          primarySwatch: Colors.blue,
          fontFamily: 'IranSans',
          accentColor: fromHex("#223862"),
          backgroundColor: Colors.white,
          primaryColor: fromHex("#223862"),
          buttonColor: fromHex("#FFB44F"),
          primaryColorLight: fromHex("#BEDDE8")),
      home: SplashScreen(),
    );
  }

  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }
}
