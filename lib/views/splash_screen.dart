import 'package:flutter/material.dart';
import 'package:gardesh_pay/controllers/splash_controller.dart';
import 'package:lottie/lottie.dart';
import 'package:get/get.dart';

class SplashScreen extends StatelessWidget {
  final splashCintroller = Get.put(SplashController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: Center(
        child: Column(
          children: [
            Spacer(),
            Container(
              child: Lottie.asset("assets/images/splash.json", height: 200),
            ),
            SizedBox(
              height: 30.0,
            ),
            Text(
              "گردش پی",
              style: TextStyle(
                  color: Theme.of(context).accentColor, fontSize: 26.0),
            ),
            Spacer(),
            CircularProgressIndicator(),
            SizedBox(height: 20.0)
          ],
        ),
      ),
    );
  }
}
